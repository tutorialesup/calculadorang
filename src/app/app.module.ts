import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalcuComponent } from './calcu/calcu.component';
import { VistaComponent } from './vista/vista.component';

@NgModule({
  declarations: [
    AppComponent,
    CalcuComponent,
    VistaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
