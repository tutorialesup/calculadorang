import { Component, OnInit } from '@angular/core';
import { IgualService } from '../igual.service';

@Component({
  selector: 'app-calcu',
  templateUrl: './calcu.component.html',
  styleUrls: ['./calcu.component.css']
})
export class CalcuComponent implements OnInit {
  operacion: string;
  o1: number;
  signo: boolean;
  mirame: string;
  resultado: number;
  ss: string;
  constructor(private logic: IgualService) { }

  ngOnInit() {
    this.resultado = 0;
    this.operacion = '0';
    this.signo = false;
    this.mirame = '0';
    this.ss = '';
    this.logic.getResultado().subscribe(r => {
      this.resultado = r;
    });

    this.op(0);
  }

  op(datos) {
    this.logic.op(datos).subscribe(s => {
      this.mirame = s;
    });
  }
  igual() {
    this.logic.getIgual();
  }

}
