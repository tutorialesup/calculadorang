import { Component, OnInit } from '@angular/core';
import { IgualService } from '../igual.service';
@Component({
  selector: 'app-vista',
  templateUrl: './vista.component.html',
  styleUrls: ['./vista.component.css']
})
export class VistaComponent implements OnInit {
  result: string;
  constructor(private igual: IgualService) { }

  ngOnInit() {
    this.result = '';
    this.igual.getResultado().subscribe(x => {
      this.result = this.result + x + ' \n';
    });
  }

}
